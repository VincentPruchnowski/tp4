# TP4


## Objectif:
Déployer/tester le build d'une application dans différents environnements: test, dev et prod.


### Description des fichiers fournis:
Terraform
- main.tf: ce fichier contient la configuration principale de Terraform, y compris la définition des ressources comme les instances Google Compute Engine.
- variables.tf: déclare les variables que j'utilise dans ma configuration Terraform et spécifie des détails sur leur type, description...
- terraform.tfvars: ce fichier est utilisé pour définir les valeurs spécifiques des variables déclarées dans variables.tf et ainsi rendre personnalisable l'execution de Terraform sans à avoir à modifier le fichier principal de configuration.
- versions.tf: spécifie la version minimale de Terraform requise.
- providers.tf: fichier de configuration du backend pour utiliser Google Cloud Storage (GCS) et utiliser le bucket "vincent-tp4-bucket" qui a été créé au préalable.


YAML Files
- dev.yaml: Fichier de configuration YAML pour l'environnement de développement.
- test.yaml: Fichier de configuration YAML pour l'environnement de test.
- prod.yaml: Fichier de configuration YAML pour l'environnement de production.

devops folder:
- Contient le Dockerfile pour le build de l'application Python.

docker-test:
- Contient les tests à effectuer sur l'application Python.
- Le docker-compose va nous permettre d'executer nos tests en local pour s'assurer que tous les services fonctionnent correctement ensemble, mais aussi dans des environnements cloud pour déployer des applications dans des environnements de test, de préproduction ou de production.

app Folder:
- Contient l'application en Python.

CD folder:
- Contient les trois pipelines pour les différents environnements d'execution.


#### Description des pré-requis:
- Système Linux/Windows
- Docker
- Terraform
- Compte GCP
- Compte Gitlab

#### Configuration des variables Gitlab:
Configurer les variables dans la librairie Gitlab pour pouvoir effectuer les actions de déploiement.
Il est possible de créer une librairie dédiée par environnement: int, uat ou prod afin de classifier et d'éviter d'écraser des variables durant un build au lancement d'une CI.

Créer les variables: 


    $CI_REGISTRY_USER : c'est le nom d'utilisateur utilisé pour l'authentification auprès du registre Docker.

    $CI_JOB_TOKEN : il s'agit d'un jeton d'accès unique généré par GitLab pour chaque travail (job) CI/CD. Il est utilisé comme mot de passe lors de l'authentification.

    $CI_REGISTRY : Cette variable contient l'URL du registre Docker associé au projet GitLab.

    $GOOGLE_CREDENTIALS: cette variable contient la clé JSON nécessaire pour authentifier le pipeline avec Google Cloud Platform.
    Elle est créée dans le panneau de navigation, séléction "IAM & admin" > "Service accounts".
    Dans les pipelines, la ligne echo "$GOOGLE_CREDENTIALS" | base64 -d > /tmp/keyfile.json décode la variable GOOGLE_CREDENTIALS (qui a été encodée en base64 pour des raisons de sécurité) et la sauvegarde dans un fichier JSON local.


###### Compte GCP:
Compte GCP: Vous devez disposer d'un compte Google Cloud Platform avec les autorisations nécessaires pour créer des ressources.
Service Account Key: nous pouvons télécharger la clé JSON du compte de service GCP et la stocker en toute sécurité dans un dossier .private par exemple.
/!\ Attention de ne pas l'envoyer sur le repository.


###### Terraform:
Les dossiers sont organisés de façon modulaire avec des modules distincts pour chaque composants: ici instances, network,...
Utiliser les commandes Terraform: terraform init, plan et apply pour déclencher les actions.

###### Pipeline CD
- Pipeline GitLab pour l'Environnement de Développement (dev)
    Le pipeline dev.yaml est déclenché lorsqu'un push est effectué sur la branche dev dont l'objectif est de vérifier si l'image Docker contient des vulnérabilités.
    Pour cela, Docker Scout va détecter d'éventuelles vulnérabilités et effectuer un Push Docker Image to GCR.
    Nous allons donc nous authentifier à Google Cloud afin de push l'image Docker vers Google Container Registry.
    Le déploiement se fait à l'aide de Terraform afin de déployer l'infrastructure et de la rendre modulaire en configurant les variables d'environnements.


- Pipeline GitLab pour l'Environnement de Test (test)
    Le pipeline test.yaml est déclenché lorsqu'un push est effectué sur la branche test. 
    Docker-compose va nous permettre d'effectuer nos tests donc on va l'installer et le déclencher sur nos instances de tests.


- Pipeline GitLab pour l'Environnement de Production (prod)
    Le pipeline prod.yaml est déclenché lorsqu'un push est effectué sur la branche prod et va effectuer le déploiement de l'image et du service.
    On va ensuite déployer le projet sur Google Kubernetes Engine (GKE), configurer l'environnement avec Google Cloud SDK,
    récupérer des informations du cluster GKE, et déployer l'application sur GKE à l'aide de Cloud Build.
    Cloud Build peut être utilisé pour automatiser le déploiement continu, où les applications sont automatiquement déployées dans des environnements de production après avoir passé les tests nécessaires.


Je peux également déclencher mes pipelines manuellement si je veux exécuter des steps précises ou alors, stipuler des conditions de déclenchement.
Je vais travailler alors sur un clone de la branche sur laquelle je vais effectuer un Pull Request afin d'attribuer à des reviewers les droits pour valider ma PR car je ne pourrais pas push mon code directemment sur cette branche par sécurité.

Par exemple, je peux configurer dans un pipeline ce type de configuration:

Branches:

    develop -> Intégration

    master -> uat 

    master -> production

Tout va dépendre de la configuration de notre projet. 
En paramétrant mes stages pour chaque environnement, je controle les déclenchements afin d'éviter des erreurs et de respecter des normes de déploiements.

Je peux implémenter des actions afin d'effectuer un build-template.yml et un push-template.yml des templates respectifs pour chacun de mes environnements dans un seul pipeline.

Et dans un script, builder dans une seule image avec la syntaxe appropriée.

Il est possible de faire du multi stages à partir de repos différents aussi.

De manière générale, un repos pour la CI, un autre pour la CD et un autre pour l'image HELM que l'on déploie en production sur un cluster Kubernetes.
