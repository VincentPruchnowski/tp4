terraform {
  backend "gcs" {
    bucket = "vincent-tp4-bucket" # Change me
    prefix = "terraform-test-prod/state"
  }
}