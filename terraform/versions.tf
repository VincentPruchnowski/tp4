terraform {
  required_version = ">= 0.13"
  backend "gcs" {
    bucket = "vincent-tp4-bucket" # Change me
    prefix = "terraform-env-base/tfstate"
  }
  
  required_providers {

    google = {
      source  = "hashicorp/google"
      version = ">= 3.64, < 5.0.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.64, < 5.0.0"
    }
  }

  provider_meta "google" {
    module_name = "blueprints/terraform/path/to/your/google-runner-module"
  }

  provider_meta "google-beta" {
    module_name = "blueprints/terraform/path/to/your/google-runner-module"
  }
}