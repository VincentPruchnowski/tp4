variable "gcp_region" {
  description = "The GCP region to deploy the runner into."
}

variable "docker-repo" {
  description = "Dockerhub repository name"
}