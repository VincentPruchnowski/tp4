variable "project_id" {
  type = string
  description = "ID du projet GCP"
}

variable "region" {
  type = string
  description = "Région GCP"
  default     = "us-west1"
}

variable "zone" {
  type        = string
  default     = "us-west1-a" 
  description = "The GCP zone to deploy the runner into."
}

variable "service_account_key_path" {
  description = "Chemin vers la clé du compte de service GCP"
}

variable "ssh_public_key_path" {
  description = "Chemin vers la clé publique SSH pour l'authentification"
}

        #####################################################################################################
        # Artifact Registry

variable "docker-repo" {
  description = "DockerHub repository name"
  default = "docker-repo" 
}

variable "machine_type" {
  type = string
  description = "Type de machine virtuelle GCP"
  default     = "n1-standard-1"
}

        #####################################################################################################
        # Network variables

variable "subnet_cidr" {
  description = "CIDR block for the subnetwork."
  default = "10.0.0.0/24"
}
variable "firewall_source" {
  description = "source block for the firewall."
  default = ["10.0.0.0/24"]
}
