provider "google" {
  credentials = file("var.service_account_key_path")
  project     = var.project_id
  region      = var.region
}

module "artifact" {
  source = "./artifact_registry_repo"
  gcp_region = var.region
  docker-repo = var.docker-repo
}

module "network" {
  source       = "./network"
  subnet_cidr = var.subnet_cidr
}

module "firewall" {
  source           = "./firewall"
  network_self_link = module.network.network_self_link
  firewall_source = var.firewall_source
}
