variable "gcp_project" {
  type        = string
  default     = "triple-router-405313"
  description = "The GCP project to deploy the runner into."
}
variable "gcp_zone" {
  type        = string
  default     = "europe-west1-c"
  description = "The GCP zone to deploy the runner into."
}

variable "gcp_region" {
  type        = string
  default     = "europe-west1"
  description = "The GCP region to deploy the runner into."
}

##########################################  Instances variables  ##########################################


variable "dev" {
  description = "Instance dev app python"
  default = "python-dev-instance"
}

variable "machine" {
  description = "Machine type"
  default = "e2-medium"
}

variable "sa_email" {
  description = "Service Account email"
  default = "terraform@triple-router-405313.iam.gserviceaccount.com"
}

variable "subnetwork" {
  description = "Subnetwork name"
  default = "my-subnetwork"
}