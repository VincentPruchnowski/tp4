terraform {
  backend "gcs" {
    bucket = "vincent-tp4-bucket"
    prefix = "terraform-env-dev/state"
  }
}